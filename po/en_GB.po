# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Andi Chandler <andi@gowling.com>, 2014-2016
# Billy Humphreys <sysop@enderbbs.enderservices.tk>, 2014
# newharha ehrara <dbybgohg@yomail.info>, 2015
# Richard Shaylor <rshaylor@me.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-31 10:43+0200\n"
"PO-Revision-Date: 2016-05-27 10:12+0000\n"
"Last-Translator: carolyn <carolyn@anhalt.org>\n"
"Language-Team: English (United Kingdom) (http://www.transifex.com/otf/"
"torproject/language/en_GB/)\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor is ready."

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "You can now access the Internet"

#: config/chroot_local-includes/etc/whisperback/config.py:65
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"

#: config/chroot_local-includes/usr/local/bin/electrum:17
msgid "Persistence is disabled for Electrum"
msgstr "Persistence is disabled for Electrum"

#: config/chroot_local-includes/usr/local/bin/electrum:19
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."

#: config/chroot_local-includes/usr/local/bin/electrum:21
msgid "Do you want to start Electrum anyway?"
msgstr "Do you want to start Electrum anyway?"

#: config/chroot_local-includes/usr/local/bin/electrum:23
#: config/chroot_local-includes/usr/local/bin/icedove:37
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Launch"

#: config/chroot_local-includes/usr/local/bin/electrum:24
#: config/chroot_local-includes/usr/local/bin/icedove:38
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Exit"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:136
msgid "OpenPGP encryption applet"
msgstr "OpenPGP encryption applet"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:139
msgid "Exit"
msgstr "Exit"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:141
msgid "About"
msgstr "About"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:192
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Encrypt Clipboard with _Passphrase"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:195
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Sign/Encrypt Clipboard with Public _Keys"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:200
msgid "_Decrypt/Verify Clipboard"
msgstr "_Decrypt/Verify Clipboard"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:204
msgid "_Manage Keys"
msgstr "_Manage Keys"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:208
msgid "_Open Text Editor"
msgstr "_Open Text Editor"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:252
msgid "The clipboard does not contain valid input data."
msgstr "The clipboard does not contain valid input data."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:303
#: config/chroot_local-includes/usr/local/bin/gpgApplet:305
#: config/chroot_local-includes/usr/local/bin/gpgApplet:307
msgid "Unknown Trust"
msgstr "Unknown Trust"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:309
msgid "Marginal Trust"
msgstr "Marginal Trust"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:311
msgid "Full Trust"
msgstr "Full Trust"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:313
msgid "Ultimate Trust"
msgstr "Ultimate Trust"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:366
msgid "Name"
msgstr "Name"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:367
msgid "Key ID"
msgstr "Key ID"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:368
msgid "Status"
msgstr "Status"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:400
msgid "Fingerprint:"
msgstr "Fingerprint:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:403
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "User ID:"
msgstr[1] "User IDs:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:433
msgid "None (Don't sign)"
msgstr "None (Don't sign)"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:496
msgid "Select recipients:"
msgstr "Select recipients:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:504
msgid "Hide recipients"
msgstr "Hide recipients"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:507
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:513
msgid "Sign message as:"
msgstr "Sign message as:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:517
msgid "Choose keys"
msgstr "Choose keys"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:557
msgid "Do you trust these keys?"
msgstr "Do you trust these keys?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:560
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "The following selected key is not fully trusted:"
msgstr[1] "The following selected keys are not fully trusted:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:578
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Do you trust this key enough to use it anyway?"
msgstr[1] "Do you trust these keys enough to use them anyway?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:591
msgid "No keys selected"
msgstr "No keys selected"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:593
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:621
msgid "No keys available"
msgstr "No keys available"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:623
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"You need a private key to sign messages or a public key to encrypt messages."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:751
msgid "GnuPG error"
msgstr "GnuPG error"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:772
msgid "Therefore the operation cannot be performed."
msgstr "Therefore the operation cannot be performed."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:822
msgid "GnuPG results"
msgstr "GnuPG results"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:828
msgid "Output of GnuPG:"
msgstr "Output of GnuPG:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:853
msgid "Other messages provided by GnuPG:"
msgstr "Other messages provided by GnuPG:"

#: config/chroot_local-includes/usr/local/bin/icedove:27
msgid "The <b>Claws Mail</b> persistence feature is activated."
msgstr "The <b>Claws Mail</b> persistence feature is activated."

#: config/chroot_local-includes/usr/local/bin/icedove:29
msgid ""
"If you have emails saved in <b>Claws Mail</b>, you should <a href='https://"
"tails.boum.org/doc/anonymous_internet/claws_mail_to_icedove'>migrate your "
"data</a> before starting <b>Icedove</b>."
msgstr ""
"If you have emails saved in <b>Claws Mail</b>, you should <a href='https://"
"tails.boum.org/doc/anonymous_internet/claws_mail_to_icedove'>migrate your "
"data</a> before starting <b>Icedove</b>."

#: config/chroot_local-includes/usr/local/bin/icedove:34
msgid ""
"If you already migrated your emails to <b>Icedove</b>, you should <a "
"href='https://tails.boum.org/doc/anonymous_internet/"
"claws_mail_to_icedove#delete'>delete all your <b>Claws Mail</b> data</a> to "
"remove this warning."
msgstr ""
"If you already migrated your emails to <b>Icedove</b>, you should <a "
"href='https://tails.boum.org/doc/anonymous_internet/"
"claws_mail_to_icedove#delete'>delete all your <b>Claws Mail</b> data</a> to "
"remove this warning."

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:71
msgid "Restart"
msgstr "Restart"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:74
msgid "Power Off"
msgstr "Power Off"

#: config/chroot_local-includes/usr/local/bin/tails-about:16
msgid "not available"
msgstr "not available"

#: config/chroot_local-includes/usr/local/bin/tails-about:19
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:24
msgid "The Amnesic Incognito Live System"
msgstr "The Amnesic Incognito Live System"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Build information:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:27
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "About Tails"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:118
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:124
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:128
msgid "Your additional software"
msgstr "Your additional software"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:119
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:129
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:125
msgid "The upgrade was successful."
msgstr "The upgrade was successful."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Synchronizing the system's clock"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Failed to synchronize the clock!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:146
msgid "This version of Tails has known security issues:"
msgstr "This version of Tails has known security issues:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:156
msgid "Known security issues"
msgstr "Known security issues"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Network card ${nic} disabled"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "All networking disabled"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "error:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:25
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Error"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:45
msgid ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"
msgstr ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:67
msgid "Warning: virtual machine detected!"
msgstr "Warning: virtual machine detected!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:69
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:72
msgid "Warning: non-free virtual machine detected!"
msgstr "Warning: non-free virtual machine detected!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:74
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Both the host operating system and the virtualisation software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualisation "
"software."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:79
msgid "Learn more"
msgstr "Learn more"

#: config/chroot_local-includes/usr/local/bin/tor-browser:40
msgid "Tor is not ready"
msgstr "Tor is not ready"

#: config/chroot_local-includes/usr/local/bin/tor-browser:41
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor is not ready. Start Tor Browser anyway?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:42
msgid "Start Tor Browser"
msgstr "Start Tor Browser"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Cancel"
msgstr "Cancel"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Do you really want to launch the Unsafe Browser?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "Starting the Unsafe Browser..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "This may take a while, so please be patient."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "Shutting down the Unsafe Browser..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Failed to restart Tor."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Unsafe Browser"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:92
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
msgid ""
"NetworkManager passed us garbage data when trying to deduce the clearnet DNS "
"server."
msgstr ""
"NetworkManager passed us garbage data when trying to deduce the clearnet DNS "
"server."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:114
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:122
msgid "Failed to setup chroot."
msgstr "Failed to setup chroot."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:128
msgid "Failed to configure browser."
msgstr "Failed to configure browser."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:133
msgid "Failed to run browser."
msgstr "Failed to run browser."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:34
msgid "I2P failed to start"
msgstr "I2P failed to start"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:35
msgid ""
"Something went wrong when I2P was starting. Check the logs in /var/log/i2p "
"for more information."
msgstr ""
"Something went wrong when I2P was starting. Check the logs in /var/log/i2p "
"for more information."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:52
msgid "I2P's router console is ready"
msgstr "I2P's router console is ready"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:53
msgid "You can now access I2P's router console in the I2P Browser."
msgstr "You can now access I2P's router console in the I2P Browser."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:58
msgid "I2P is not ready"
msgstr "I2P isn't ready."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:59
msgid ""
"Eepsite tunnel not built within six minutes. Check the router console in the "
"I2P Browser or the logs in /var/log/i2p for more information. Reconnect to "
"the network to try again."
msgstr ""
"Eepsite tunnel not built within six minutes. Check the router console in the "
"I2P Browser or the logs in /var/log/i2p for more information. Reconnect to "
"the network to try again."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:71
msgid "I2P is ready"
msgstr "I2P is now ready"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:72
msgid "You can now access services on I2P."
msgstr "You can now access services on I2P."

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Report an error"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Tails documentation"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Learn how to use Tails"

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:1
msgid "Anonymous overlay network browser"
msgstr "Anonymous overlay network browser."

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:2
msgid "I2P Browser"
msgstr "I2P Browser"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Learn more about Tails"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Browser"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonymous Browser"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Browse the World Wide Web without anonymity"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Unsafe Web Browser"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Tails specific tools"
