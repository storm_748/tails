[[!meta title="Tails August 2016 report"]]

[[!toc levels=2]]

<div class="caution">
<strong>Deadline: 2016-09-05</strong>
</div>

<div class="note">
Deliverable identifiers and descriptions are not free-form: they must
be copy'n'pasted as-is from the proposal sent to the sponsor.

<pre>
## A.n. description of subsection

- A.n.m. description of deliverable: ticket numbers

  status summary:

  * what was done
  * what is the outcome (how it makes Tails better)
  * what was not done, and why
</pre>
</div>

[Last month's activity on Redmine](https://labs.riseup.net/code/projects/tails/issues?query_id=208)
can be helpful.

This reports covers the activity of Tails in August 2016.

Everything in this report is public.

# A. Replace Claws Mail with Icedove

- A.1.1. Secure the Icedove autoconfig wizard

XXX

- A.1.2. Make our improvements maintainable for future versions of Icedove

XXX

# B. Improve our quality assurance process

## B.3. Extend the coverage of our test suite

- B.3.11. Fix newly identified issues to make our test suite more robust and faster

XXX

- B.3.12. Reliably wait for post-Greeter hooks ([[!tails_ticket 5666]])

XXX

- B.3.14. Write tests for incremental upgrades ([[!tails_ticket 6309]])

XXX

# C. Scale our infrastructure

## C.1. Change in depth the infrastructure of our pool of mirrors:

- C.1.2. Write & audit the code that makes the redirection decision from our website

XXX

- C.1.6. Adjust download documentation to point to the mirror pool dispatcher's URL

XXX

- C.1.8. Clean up the remainers of the old mirror pool setup

XXX

# E. Release management

## E.1.12. Tails 2.5

XXX
